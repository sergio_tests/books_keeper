from fastapi import UploadFile

import book_app.config as cfg
from book_app.db.mongo_model import BookDBModel
from book_app.shemas.books import BookList, Book


class BookController:
    def __init__(self):
        self.db = BookDBModel(cfg.MONGO_URI)

    async def get_books(self, limit: int, offset: int) -> BookList:
        total_books = await self.db.count()
        books = await self.db.load(limit=limit, offset=offset)
        response = BookList(
            items=books,
            count=len(books),
            total=total_books,
            offset=offset,
            limit=limit,
        )
        return response

    async def create_book(
            self,
            title: str,
            description: str,
            cover: UploadFile,
    ) -> Book:
        file_location = cfg.IMG_DIR / cover.filename
        with open(file_location, "wb+") as file_object:
            file_object.write(cover.file.read())

        cover_url = f"{cfg.IMG_URL_PATH}/{cover.filename}"
        new_book = Book(title=title, description=description, cover=str(cover_url))
        book = await self.db.create(new_book)
        return book


__all__ = ["BookController"]
