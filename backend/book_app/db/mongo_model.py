from typing import List

import bson
from databases import DatabaseURL
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorCollection
from pydantic import BaseModel

from book_app.shemas.books import Book


def stringify_bson_ids(docs: List[dict], fields: List[str]):
    for doc in docs:
        for f in fields:
            value = doc.get(f)
            if isinstance(value, bson.ObjectId):
                doc[f] = str(value)
    return docs


class MongoDBModel:
    __collection_name__ = None
    __item_schema__ = None
    __item_object_id_fields__ = ['_id']

    def __init__(self, conn_uri: DatabaseURL = None):
        self.conn = conn_uri
        self.client = AsyncIOMotorClient(str(self.conn))
        self.db = self.client[self.conn.database]

        self.chunk_size = 100
        self._collection = None

    async def startup_init(self):
        list_collection_names = await self.db.list_collection_names()
        if self.collection_name not in list_collection_names:
            await self.db.create_collection(self.collection_name)

    @property
    def collection_name(self) -> str:
        assert self.__collection_name__
        return self.__collection_name__

    async def prepare(self):
        if self._collection is None:
            self._collection = self.db[self.collection_name]

    @property
    def collection(self) -> AsyncIOMotorCollection:
        return self._collection

    def make_object(self, doc: dict):
        if doc is None:
            return None

        return self.__item_schema__.parse_obj(doc)

    async def load_raw(
            self,
            query: dict = None,
            projection: list = None,
            limit: int = None,
            offset: int = None,
    ) -> List[dict]:
        await self.prepare()
        query = query or {}

        if limit == 0:
            return []

        cursor = self.collection.find(query, projection)
        if limit:
            cursor = cursor.limit(limit)
        if offset:
            cursor = cursor.skip(offset)

        result = []
        while True:
            chunk = await cursor.to_list(self.chunk_size)
            if len(chunk) == 0:
                break
            result += chunk

        result = stringify_bson_ids(result, self.__item_object_id_fields__)
        return result

    async def load(
            self,
            query: dict = None,
            projection: list = None,
            limit: int = None,
            offset: int = None,
    ):
        docs = await self.load_raw(query, projection, limit, offset)

        if not self.__item_schema__:
            return docs

        result = []
        for doc in docs:
            obj = self.make_object(doc)
            result.append(obj)

        return result

    async def load_one(
            self,
            query: dict = None,
            projection: list = None,
    ):
        items = await self.load(query, projection)

        if len(items) == 0:
            return None

        return items[0]

    async def count(self, query: dict = None) -> int:
        await self.prepare()
        query = query or {}
        total = await self.collection.count_documents(query)
        return total

    async def create(self, obj: BaseModel):
        assert isinstance(obj, BaseModel), f'create accepts only pydantic objects, but got {type(obj)}'
        await self.prepare()
        doc = obj.dict()
        insert_res = await self.collection.insert_one(doc)
        result = await self.load_one({'_id': insert_res.inserted_id})
        return result


class BookDBModel(MongoDBModel):
    __collection_name__ = 'books'
    __item_schema__ = Book


__all__ = [
    MongoDBModel,
    BookDBModel,
]
