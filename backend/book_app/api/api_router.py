from fastapi import APIRouter, Depends, Query, UploadFile, File, Form

from book_app.controllers.books_controller import BookController
from book_app.shemas.books import BookList, Book


api_router = APIRouter()


@api_router.get("/books/list/", response_model=BookList)
async def get_books(
        limit: int = Query(10, gt=0),
        offset: int = Query(0, ge=0),
        controller: BookController = Depends(BookController),
) -> BookList:
    books: BookList = await controller.get_books(limit, offset)
    return books


@api_router.post("/books/create/", status_code=201, response_model=Book)
async def create_book(
        title: str = Form(...),
        description: str = Form(...),
        cover: UploadFile = File(...),
        controller: BookController = Depends(BookController),
) -> Book:
    book: Book = await controller.create_book(title, description, cover)
    return book


__all__ = [
    'api_router',
]
