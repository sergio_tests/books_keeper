from typing import List, Optional, TypeVar, Generic

from pydantic import BaseModel, Field
from pydantic.generics import GenericModel

T = TypeVar('T')


class GenericPagination(GenericModel):
    limit: Optional[int] = 0
    count: Optional[int] = 0
    offset: Optional[int] = 0
    total: Optional[int] = 0


class GenericItemsPagination(GenericPagination, Generic[T]):
    items: List[T]


class Book(BaseModel):
    title: str = Field(..., min_length=1, max_length=255)
    description: str = Field(..., min_length=1)
    cover: str = Field(..., min_length=1)


BookList = GenericItemsPagination[Book]


__all__ = [
    "Book",
    "BookList",
]
