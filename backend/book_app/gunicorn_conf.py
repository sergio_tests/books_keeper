import json
import multiprocessing
import os


host = os.getenv("HOST", "0.0.0.0")
port = os.getenv("PORT", "8000")
bind_env = os.getenv("BIND", None)
use_bind = bind_env if bind_env else f"{host}:{port}"

workers_per_core_str = os.getenv("WORKERS_PER_CORE", "1")
workers_per_core = float(workers_per_core_str)
max_workers_str = os.getenv("MAX_WORKERS")
use_max_workers = int(max_workers_str) if max_workers_str else None

cores = multiprocessing.cpu_count()
default_web_concurrency = workers_per_core * cores
web_concurrency = max(int(default_web_concurrency), 2)
if use_max_workers:
    web_concurrency = min(web_concurrency, use_max_workers)

use_loglevel = os.getenv("LOG_LEVEL", "info")
use_accesslog = os.getenv("ACCESS_LOG", "-") or None
use_errorlog = os.getenv("ERROR_LOG", "-") or None
graceful_timeout_str = os.getenv("GRACEFUL_TIMEOUT", "120")
timeout_str = os.getenv("TIMEOUT", "120")
keepalive_str = os.getenv("KEEP_ALIVE", "5")

# Gunicorn config variables
loglevel = use_loglevel
workers = web_concurrency
bind = use_bind
errorlog = use_errorlog
accesslog = use_accesslog
graceful_timeout = int(graceful_timeout_str)
timeout = int(timeout_str)
keepalive = int(keepalive_str)


# For debugging
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    "graceful_timeout": graceful_timeout,
    "timeout": timeout,
    "keepalive": keepalive,
    "errorlog": errorlog,
    "accesslog": accesslog,
    # Additional, non-gunicorn variables
    "workers_per_core": workers_per_core,
    "use_max_workers": use_max_workers,
    "host": host,
    "port": port,
}
print('Gunicorn config:', json.dumps(log_data))
