import logging
import os
from pathlib import Path

from databases import DatabaseURL
from starlette.config import Config
from starlette.datastructures import URLPath

# Load environment variables from .env file
APP_ROOT = Path(__file__).resolve().absolute().parent.parent
CONFIG_ROOT = Path(os.environ.get('APP_CONFIG_ROOT') or APP_ROOT)
ENV_FILE = CONFIG_ROOT / ".env"
CONFIG = Config(ENV_FILE)

DEBUG: bool = CONFIG.get('APP_DEBUG', bool, default=False)
IMG_URL_PATH: URLPath = CONFIG.get('APP_IMG_URL_PATH', URLPath)
IMG_DIR: Path = CONFIG.get('APP_IMG_DIR', Path)
MONGO_URI: DatabaseURL = CONFIG.get('APP_MONGO_URI', DatabaseURL)

logging.basicConfig(
    level=logging.ERROR,
    format='[%(asctime)s %(levelname)s] %(message)s',
    datefmt='%Y-%m-%d %H:%M'
)
