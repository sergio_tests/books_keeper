from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from pydantic import ValidationError
from starlette.middleware.cors import CORSMiddleware

import book_app.config as cfg
from book_app.api.api_router import api_router
from book_app.http_error import return_http_error, process_error, HttpException


app = FastAPI(
    title='Book API Server',
    version='1.0.0',
    docs_url='/api/v1/docs',
    redoc_url='/api/v1/redoc',
    openapi_url='/api/v1/openapi.json'
)

origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.exception_handler(Exception)(process_error)
app.exception_handler(HttpException)(return_http_error)
app.exception_handler(ValidationError)(process_error)
app.exception_handler(RequestValidationError)(process_error)

app.include_router(api_router, prefix='/api/v1')

if cfg.DEBUG:
    from starlette.staticfiles import StaticFiles
    app.mount("/static", StaticFiles(directory=cfg.IMG_DIR), name="static")


@app.on_event("startup")
async def startup_event():
    from book_app.db.mongo_model import BookDBModel
    await BookDBModel(cfg.MONGO_URI).startup_init()


def run_server():
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8000, reload=cfg.DEBUG)


if __name__ == "__main__":
    run_server()
