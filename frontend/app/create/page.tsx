'use client';

import { useState } from 'react';
import { useRouter } from 'next/navigation';

const backendHost = process.env.NEXT_PUBLIC_BACKEND_HOST;

export default function CreateBookPage() {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [cover, setCover] = useState<File | null>(null);
  const router = useRouter();

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    if (cover) {
      formData.append('cover', cover);
    }

    try {
      const res = await fetch(`${backendHost}/api/v1/books/create/`, {
        method: 'POST',
        body: formData,
      });

      if (res.ok) {
        router.push('/books');
      } else {
        console.error('Failed to create book');
      }
    } catch (error) {
      console.error('Error creating book:', error);
    }
  };

  return (
      <div className="container mx-auto p-4">
        <h1 className="text-2xl font-bold mb-4">Create a New Book</h1>
        <form onSubmit={handleSubmit} className="flex flex-col space-y-4">
          <input
              type="text"
              placeholder="Title"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
              className="px-4 py-2 border rounded-md dark:bg-gray-800 dark:border-gray-700 dark:text-white"
              required
          />
          <textarea
              placeholder="Description"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              className="px-4 py-2 border rounded-md dark:bg-gray-800 dark:border-gray-700 dark:text-white"
              required
          />
          <input
              type="file"
              accept="image/*"
              onChange={(e) => {
                if (e.target.files && e.target.files[0]) {
                  setCover(e.target.files[0]);
                }
              }}
              className="px-4 py-2 border rounded-md"
              required
          />
          <button
              type="submit"
              className="px-4 py-2 border rounded-md dark:bg-gray-800 dark:border-gray-700 dark:text-white"
          >Create Book</button>
        </form>
      </div>
  );
}
