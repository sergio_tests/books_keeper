'use client';

import { useEffect, useState, useMemo } from 'react';
import Book from '../../components/Book';

const backendHost = process.env.NEXT_PUBLIC_BACKEND_HOST;
const imgHost = process.env.NEXT_PUBLIC_IMG_HOST || backendHost;

type BookType = {
  title: string;
  description: string;
  cover: string;
};

export default function BooksPage() {
  const [books, setBooks] = useState<BookType[]>([]);
  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchBooks = async () => {
      const limit = 10; // Количество книг на страницу
      const offset = (page - 1) * limit;

      try {
        const res = await fetch(`${backendHost}/api/v1/books/list/?limit=${limit}&offset=${offset}`);
        const data = await res.json();

        data.items = data.items.map((item: BookType) => {
          return {
            ...item,
            cover: imgHost + item.cover
          };
        });

        setBooks(data.items);
        setTotalPages(Math.ceil(data.total / limit));
      } catch (error) {
        console.error('Failed to fetch books:', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchBooks();
  }, [page]);

  const memoizedBooks = useMemo(() => books, [books]);

  return (
    <div className="container mx-auto p-4">
      {isLoading ? (
        <p>Loading...</p>
      ) : memoizedBooks.length === 0 ? (
        <p>The book list is empty.</p>
      ) : (
        <>
          <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4">
            {memoizedBooks.map((book, index) => (
              <Book key={index} {...book} />
            ))}
          </div>
          <div className="flex justify-center mt-4">
            <button onClick={() => setPage(page - 1)} disabled={page === 1} className="px-4 py-2 bg-blue-500 text-white rounded-md">Previous</button>
            <span className="mx-2">{page} / {totalPages}</span>
            <button onClick={() => setPage(page + 1)} disabled={page === totalPages} className="px-4 py-2 bg-blue-500 text-white rounded-md">Next</button>
          </div>
        </>
      )}
    </div>
  );
}
