import 'dotenv/config';

/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: [
            process.env.NEXT_PUBLIC_BACKEND_HOST
                .replace(/^https?:\/\//, '')
                .replace(/:\d+$/, ''),
            process.env.NEXT_PUBLIC_IMG_HOST
                .replace(/^https?:\/\//, '')
                .replace(/:\d+$/, '')
        ],
    },
    env: {
        NEXT_PUBLIC_BACKEND_HOST: process.env.NEXT_PUBLIC_BACKEND_HOST,
        NEXT_PUBLIC_IMG_HOST: process.env.NEXT_PUBLIC_IMG_HOST,
    },
};

export default nextConfig;
