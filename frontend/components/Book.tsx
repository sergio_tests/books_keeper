import Image from 'next/image';
import React from 'react';

type BookProps = {
  title: string;
  description: string;
  cover: string;
};

const Book: React.FC<BookProps> = ({ title, description, cover }) => (
  <div className="border p-4 rounded-md shadow-sm">
    <Image src={cover} alt={title} width={150} height={200} className="object-cover" />
    <h2 className="text-xl font-bold mt-2">{title}</h2>
    <p className="text-gray-700">{description}</p>
  </div>
);

export default React.memo(Book);
