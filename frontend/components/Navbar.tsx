import Link from 'next/link';

const Navbar: React.FC = () => (
  <nav className="bg-gray-800 p-4">
    <div className="container mx-auto flex justify-between">
      <Link href="/books" className="text-white">Book list</Link>
      <Link href="/create" className="text-white">Create book</Link>
    </div>
  </nav>
);

export default Navbar;
