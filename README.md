# book_keeper


Тестовое задание, одностраничное приложение, в котором пользователь может посмотреть список книг или создать книгу.

Сервер реализован на базе python-фреймворка [fastapi](https://fastapi.tiangolo.com/), фронтенд на базе react+nextjs+tailwindcss с mongodb в качестве хранилища.  


## Запуск в docker

Простейшим способом запуска проекта в dev окружении является docker-compose. В составе проекта идет `docker-compose.yml` который позволяет быстро запустить проект для разработки.

_При первом использовании нужно настроить .env файл_

~~~
# Копируем шаблоны .env файлов 
cp backend/.dev.env backend/.env
cp frontend/.dev.env frontend/.env.local

# ... и редактируем нужные настройки
~~~

**Сборка и запуск контейнера**

~~~shell
# Сборка
docker-compose build

# Блокирующий запуск в котором видны логи
docker-compose up

# ... или фоновый запуск
docker-compose up -d 
~~~


## Установка без docker

1.  Клонировать проект
2.  Скопировать файлы окружения `.dev.env -> .env`
3.  Установить зависимости на сервере `cd backend; pip3 install -r requirements.txt`
4.  Запуск сервера: `gunicorn -k uvicorn.workers.UvicornWorker -c ./gunicorn_conf.py server:app`
4.1.  Запуск сервера для разработки: `uvicorn server:app --reload`
5.  Установить зависимости на фронтенде `cd frontend; npm install`
6.  Запуск фронтенда: `npm run dev`
7.  Открыть в браузере адрес `http://localhost:3000/`


## Конфигурация

Пример настройки в файле `backend/.env`:
```
APP_DEBUG=1
APP_IMG_URL=http://localhost:8000/static
APP_IMG_DIR=/tmp/
APP_MONGO_URI=mongodb://localhost:27017/book_keeper
```

Пример настройки в файле `frontend/.env.local`:
```
NEXT_PUBLIC_BACKEND_HOST=http://localhost:8000
NEXT_PUBLIC_IMG_HOST=http://localhost:8000
```
